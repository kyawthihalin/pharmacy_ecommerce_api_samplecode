<?php

use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\CartController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AppAddressController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PaymentTypeController;
use App\Http\Controllers\ShopProductController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\AccountDetailController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ShippingAddressController;
use App\Http\Controllers\ShopProductBalanceController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\ShopProductCategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('register')->controller(RegisterController::class)->group(function () {
    Route::post('otp/get', 'getOtp');
    Route::post('otp/verify', 'verifyOtp')->middleware(['apptoken']);
});
Route::prefix('password')->controller(ForgotPasswordController::class)->group(function () {
    Route::post('forgot', 'forgot');
    Route::post('otp/verify', 'verifyOtp')->middleware(['apptoken']);
    Route::post('update', 'update')->middleware(['apptoken']);
});
Route::post('login', [LoginController::class, 'login']);

Route::middleware(['auth:shop'])->group(function () {
    Route::post('me', [AccountDetailController::class, 'detail']);
    Route::post('shop/update', [AccountDetailController::class, 'update']);
    Route::post('logout', [LoginController::class, 'logout']);

    Route::post('categories/top', [CategoryController::class, 'topCategories']);
    Route::post('categories', [CategoryController::class, 'list']);

    Route::post('brands/feature', [BrandController::class, 'featuredBrands']);
    Route::post('brands', [BrandController::class, 'list']);

    Route::prefix('products')->controller(ProductController::class)->group(function () {
        Route::post('request', 'request');
        Route::post('/', 'list');
        Route::post('show', 'detail');
        Route::post('return', 'productReturn');
        Route::post('deals-of-week', 'dealsOfTheWeek');
        Route::post('best-selling', 'bestSelling');
        Route::post('popular', 'popular');
    });

    Route::prefix('carts')->controller(CartController::class)->group(function () {
        Route::post('/', 'list');
        Route::post('count', 'count');
        Route::post('add', 'addToCart');
        Route::post('increase', 'increase');
        Route::post('decrease', 'decrease');
        Route::post('quantity', 'quantity');
        // Route::post('save', 'save');
        Route::post('remove', 'remove');
        Route::post('coupon/verify', 'checkCoupon');
        Route::post('coupon/remove', 'removeCoupon');
        // Route::post('amount/calculate', 'calculateAmount');
    })->middleware('throttle:120,1');

    Route::prefix('shipping-addresses')->controller(ShippingAddressController::class)->group(function () {
        Route::post('/', 'list');
        Route::post('create', 'create');
        Route::post('update', 'update');
        Route::post('show', 'show');
        Route::post('default', 'default');
    });

    Route::prefix('addresses')->controller(AppAddressController::class)->group(function () {
        Route::post('cities', 'getCities');
        Route::post('townships', 'getTownships');
    });

    Route::prefix('notifications')->controller(NotificationController::class)->group(function () {
        Route::post('/', 'list');
        Route::post('unread-count', 'unreadCount');
        Route::post('read-all', 'readAll');
        Route::post('read', 'read');
        Route::post('unread', 'unread');
        Route::post('survey', 'survey');
    });

    Route::post('order', [OrderController::class, 'order']);
    Route::post('order/history', [OrderController::class, 'history']);
    Route::post('order/replace', [OrderController::class, 'replace']);
    Route::post('order/detail', [OrderController::class, 'detail']);
    Route::post('order/products', [OrderController::class, 'products']);
    
    Route::post('stocks', [StockController::class, 'list']);
    Route::post('stocks/update', [StockController::class, 'update']);
    Route::post('stocks/delete', [StockController::class, 'destroy']);
    Route::post('stocks/out', [StockController::class, 'out']);
    Route::post('stocks/add', [StockController::class, 'add']);

    Route::post('payment-type', [PaymentTypeController::class, '__invoke']);
    Route::post('banners', [BannerController::class, '__invoke']);
    
    Route::post('/shop/balances/list',[ShopProductBalanceController::class, 'list']);
    Route::post('/shop/balances/add',[ShopProductBalanceController::class, 'add']);
    Route::post('/shop/balances/edit',[ShopProductBalanceController::class, 'edit']);

    Route::post('/shop/categories',[ShopProductController::class, 'getCategory']);
    Route::post('/shop/products',[ShopProductController::class, 'getProduct']);
    Route::post('/shop/low-stock-products',[ShopProductController::class, 'getLowStockProduct']);
    Route::post('/shop/order',[ShopProductController::class, 'order']);
    Route::post('/shop/invoices',[ShopProductController::class, 'invoice']);
    Route::post('/shop/invoices/detail',[ShopProductController::class, 'invoiceDetail']);
    Route::post('/shop/revenue',[ShopProductController::class, 'revenue']);
    Route::post('/shop/tax',[ShopProductController::class, 'getTax']);

    Route::post('/shop/profit-margin/create-update',[ShopProductController::class, 'setProfitMargin']);
    Route::post('/shop/profit-margin',[ShopProductController::class, 'getProfitMargin']);

});
