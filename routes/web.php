<?php

use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {

    // Set the locale to Thai
    Carbon::setLocale('th');

    // Thai month names to English month names mapping
    $thaiToEnglishMonths = [
        'มกราคม' => 'January',
        'กุมภาพันธ์' => 'February',
        'มีนาคม' => 'March',
        'เมษายน' => 'April',
        'พฤษภาคม' => 'May',
        'มิถุนายน' => 'June',
        'กรกฎาคม' => 'July',
        'สิงหาคม' => 'August',
        'กันยายน' => 'September',
        'ตุลาคม' => 'October',
        'พฤศจิกายน' => 'November',
        'ธันวาคม' => 'December',
    ];

    // Create a Carbon instance from the Thai date
    $thaiDate = '16 พฤษภาคม 2566';
    $parts = explode(' ', $thaiDate);
    $englishMonth = $thaiToEnglishMonths[$parts[1]];
    $englishYear = $parts[2] - 543; // Convert Thai year to Western year
    $englishDate = $parts[0] . ' ' . $englishMonth . ' ' . $englishYear;

    $date = Carbon::createFromFormat('d F Y', $englishDate);

    // Format the date in English
    $englishDateFormatted = $date->format('d-m-Y'); // or any other desired format

    echo $englishDateFormatted;
});

Route::post('upload', function() {
    if(request()->has('logo')){
        $file = request()->file('logo');
        $path = 'shops/logo/shop_default.png';
        if(Storage::exists($path)){
            Storage::delete($path);
        }
        Storage::put($path, file_get_contents($file));
    }
});
