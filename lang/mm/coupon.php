<?php

return [
    'invalid' => 'Your coupon is invalid.',
    'expired' => 'Your coupon was expired.',
    'used' => 'The coupon you have attempted to use has already reached its maximum usage limit.'
];
