<?php

return [
    'success' => 'Success',
    'error' => 'Error',
    'bad_request' => 'Bad Request',
    'failed' => [
        'app_token' => [
            'expired' => 'Your session was expired, please try again.'
        ],
        'app_otp' => [
            'invalid' => 'Your OTP is invalid, please try again.',
            'expired' => 'Your OTP was expired, please try again.'
        ]
    ],
    'over_limit' => [
        'shipping_address' => 'You already reached maximum shipping address limit.'
    ],
];
