<?php

return [
    'failed' => [
        'credential' => 'Phone number or password is incorrect.',
        'shop_exist' => 'This phone number is already registered, please try loggin in.',
        'frozen' => 'Your account is currently locked, please contact customer support.',
        'approve' => 'We are currently reviewing your account. Please be patient.'
    ],
    'success' => [
        'otp' => [
            'sent' => 'We have sent an OTP to your phone number :phone_number, please enter it below.'
        ]
    ]
];
