<?php

return [
    'limit_reached' => 'You have reached maximum order limit for this product.',
    'min_product' => 'You have reached minimum order limit for this product.',
];
