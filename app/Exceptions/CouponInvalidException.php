<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class CouponInvalidException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithSuccess(data: [
            'errors' => [
                'coupon_number' => [
                    __("coupon.invalid")
                ]
            ]
        ], message: __("coupon.invalid"), code: 422);
    }
}
