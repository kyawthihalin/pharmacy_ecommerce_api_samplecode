<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class InvalidAppOtpException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithError(
            message: "global.failed.app_otp.invalid",
            code: Response::HTTP_UNAUTHORIZED
        );
    }
}
