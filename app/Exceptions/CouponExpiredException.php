<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class CouponExpiredException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithSuccess(data: [
            'errors' => [
                'coupon_number' => [
                    __("coupon.expired")
                ]
            ]
        ], message: __("coupon.expired"), code: 422);
    }
}
