<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class CouponUsedException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithSuccess(data: [
            'errors' => [
                'coupon_number' => [
                    __("coupon.used")
                ]
            ]
        ], message: __("coupon.used"), code: 422);
        return $this->responseWithError(message: "coupon.used", code: 422);
    }
}
