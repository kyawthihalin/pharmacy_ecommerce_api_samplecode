<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ExpiredAppOtpException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithError(
            message: "global.failed.app_otp.expired",
            code: Response::HTTP_UNAUTHORIZED
        );
    }
}
