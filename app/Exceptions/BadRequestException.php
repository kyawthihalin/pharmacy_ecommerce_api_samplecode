<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class BadRequestException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithError();
    }
}
