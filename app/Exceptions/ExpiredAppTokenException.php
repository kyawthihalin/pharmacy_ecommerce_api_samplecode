<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class ExpiredAppTokenException extends Exception
{
    use ApiResponse;

    public function render()
    {
        return $this->responseWithError(
            message: "global.failed.app_token.expired",
            code: Response::HTTP_GONE
        );
    }
}
