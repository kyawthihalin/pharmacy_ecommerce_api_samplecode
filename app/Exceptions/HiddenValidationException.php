<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class HiddenValidationException extends Exception
{
    use ApiResponse;
    public function __construct(private array $errors)
    {
    }

    public function render()
    {
        if (app()->environment('production')) {
            abort(400);
        }

        return $this->responseWithValidationErrors(
            errors: $this->errors,
            message: "Development validation error occured!. only bad request with status code 400 will return in production."
        );
    }
}
