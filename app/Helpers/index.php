<?php

/**
 * 
 * get random digits
 */
if (!function_exists('get_random_digit')) {
    function get_random_digit(int $length): string
    {
        return rand(pow(10, $length - 1), pow(10, $length) - 1);
    }
}

/**
 * 
 * get delicated string from salt
 */
if (!function_exists('get_random_str')) {
    function get_random_str(bool $upper_cases = true, bool $numbers = true, bool $special_charaters = false, int $length = 8): string
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz';

        if ($upper_cases) {
            $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        if ($numbers) {
            $characters .= '0123456789';
        }

        if ($special_charaters) {
            $characters .= "@#$%&*!";
        }

        $randomstr = '';
        for ($i = 0; $i < $length; $i++) {
            $randomstr .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomstr;
    }
}
