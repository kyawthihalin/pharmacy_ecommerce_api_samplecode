<?php

namespace App\Jobs;

use App\Interfaces\SmsServiceInterface;
use App\Services\Sms\BoomSmsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private string $to, private string $message)
    {
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (!config('app.app_sms')) {
            Log::channel('dev')->info("Sent sms >>> ", [
                'to' => $this->to,
                'message' => $this->message
            ]);
            return;
        }
        (new BoomSmsService())->send(to: $this->to, message: $this->message);
    }
}
