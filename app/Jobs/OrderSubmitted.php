<?php

namespace App\Jobs;

use App\Actions\SendNotification;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OrderSubmitted
{
    use Dispatchable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private Order $order)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $shop = $this->order->shop;
        $title = 'Moe Hein Gabar';
        $description = "{$this->order->order_no} အား မှာယူမှု ပြုလုပ်ပြီးပါပြီ။";

        $shop->notifications()->create([
            'title' => $title,
            'description' => $description,
            'type' => 'Notification',
            'order_id' => $this->order->id,
        ]);

        (new SendNotification())->execute([$shop->token], [
            'Type' => 'Notification',
            'title' => 'Moe Hein Gabar',
            'message' => "{$this->order->order_no} အား မှာယူမှု ပြုလုပ်ပြီးပါပြီ။",
        ]);
    }
}
