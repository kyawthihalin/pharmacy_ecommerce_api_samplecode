<?php

namespace App\Actions;

use App\Http\Resources\ProductDetailResource;
use App\Models\ShippingAddress;
use Illuminate\Support\Collection;

class GetOrderDetail
{
  public function execute()
  {
    $cart = auth()->user()->cart;
    $total_product_amount = 0;
    $total_product_discount = 0;
    $coupon_discount = 0;
    $products = new Collection();
    $total_product_quantity = 0;
    $shipping_fee = 0;
    $net_amount = 0;

    foreach ($cart->products as $cart_product) {
      $product = $cart_product->product;
      $total_product_amount += ($product->price * $cart_product->quantity);
      $total_product_quantity += $cart_product->quantity;

      $product_discount = $product->calculated_discount;
      $total_product_discount += ($product_discount * $cart_product->quantity);

      $products->push($product);
    }

    $coupon = null;
    $is_coupon_valid = false;
    if ($cart->coupon_number) {
      $validation_response = (new ValidateCoupon(coupon_number: $cart->coupon_number, throw_exception: false))->execute();
      $coupon = $validation_response['coupon'];
      $is_coupon_valid = $validation_response['is_valid'];
    }

    if ($is_coupon_valid) {
      $coupon_discount = (new GetCouponAmount())->execute(coupon: $coupon, total_amount: $total_product_amount);
    }

    $shipping_address = ShippingAddress::where('default_shipping_address', true)->first();

    if ($shipping_address) {
      $shipping_fee = $shipping_address->township->shipping_fees;
    }

    $total_discount = (float) bcadd($total_product_discount, $coupon_discount, 4);

    $grand_total =  (float) bcsub($total_product_amount, $total_discount, 4);
    $net_amount = (float) bcadd($grand_total, $shipping_fee, 4);

    return [
      'cart_detail' => ProductDetailResource::collection($products),
      'product_count' => count($products),
      'total_product_quantity' => $total_product_quantity,
      'total_product_amount' => $total_product_amount,
      'total_product_discount' => $total_product_discount,
      'coupon_number' => $coupon ? $coupon->coupon_number : null,
      'coupon_discount' => $coupon_discount,
      'shipping_fee' => $shipping_fee,
      'total_discount' => $total_discount,
      'grand_total' => $grand_total,
      'net_amount' => $net_amount,
      'is_coupon_valid' => $is_coupon_valid,
    ];
  }
}
