<?php

namespace App\Actions;

use App\Models\Coupon;

class GetCouponAmount
{
    public function execute(Coupon $coupon, float $total_amount)
    {
        $discount_amount = $coupon->amount;

        if (!$discount_amount) {
            return 0;
        }

        if ($coupon->coupon_type === 'Fixed') {
            return $discount_amount;
        }

        if ($coupon->coupon_type === 'Percent') {
            return (float) bcmul((float)bcdiv($discount_amount, 100, 4), $total_amount);
        }

        return 0;
    }
}
