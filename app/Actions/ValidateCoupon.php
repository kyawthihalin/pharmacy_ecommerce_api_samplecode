<?php

namespace App\Actions;

use App\Exceptions\BadRequestException;
use App\Exceptions\CouponExpiredException;
use App\Exceptions\CouponInvalidException;
use App\Exceptions\CouponUsedException;
use App\Models\Coupon;
use App\Models\Shop;
use Carbon\Carbon;
use Exception;

class ValidateCoupon
{
    private Coupon $coupon;
    private bool $is_valid = true;
    public function __construct(private string $coupon_number, private bool $throw_exception = true, private bool $throw_bad = false)
    {
    }

    public function execute()
    {
        $this->isCouponExist()->isCouponExpired()->isCouponReachedLimit();
        return [
            'coupon' => $this->coupon,
            'is_valid' => $this->is_valid
        ];
    }

    private function isCouponExist()
    {
        $coupon = Coupon::where('coupon_number', $this->coupon_number)->first();

        if (!$coupon) {
            $this->throwException(exception: new CouponInvalidException());
        }

        $this->coupon = $coupon;

        return $this;
    }

    private function isCouponExpired()
    {
        if (!$this->coupon->coupon_start_date && !$this->coupon->coupon_end_date) {
            return $this;
        }

        if ($this->coupon->coupon_start_date && !$this->coupon->coupon_end_date) {
            if (Carbon::parse($this->coupon->coupon_start_date)->isFuture()) {
                $this->throwException(exception: new CouponInvalidException());
            }
        }

        if (!$this->coupon->coupon_start_date && $this->coupon->coupon_end_date) {
            if (!Carbon::parse($this->coupon->coupon_end_date)->isFuture()) {
                $this->throwException(exception: new CouponExpiredException());
            }
        }

        if ($this->coupon->coupon_start_date && $this->coupon->coupon_end_date) {
            if (Carbon::parse($this->coupon->coupon_start_date)->isFuture()) {
                $this->throwException(exception: new CouponInvalidException());
            }

            if (!now()->between($this->coupon->coupon_start_date, $this->coupon->coupon_end_date)) {
                $this->throwException(exception: new CouponExpiredException());
            }
        }

        return $this;
    }

    private function isCouponReachedLimit()
    {
        $shop = Shop::find(auth()->id());

        $shop_coupon = $shop->shop_coupons()->where('coupon_id', $this->coupon->id)->first();

        if (!$shop_coupon) {
            return $this;
        }

        if ($shop_coupon->used_times >= $this->coupon->valid_times) {
            $this->throwException(exception: new CouponUsedException());
        }

        return $this;
    }

    private function throwException(Exception $exception)
    {
        $this->is_valid = false;
        if (!$this->throw_exception) {
            return;
        }
        $this->throw_bad && throw new BadRequestException();

        throw $exception;
    }
}
