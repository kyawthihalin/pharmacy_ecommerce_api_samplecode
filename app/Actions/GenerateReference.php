<?php

namespace App\Actions;

class GenerateReference
{
    public function execute(string $prefix, int $digit, int $random_int_length = 0)
    {
        $random_int = "";
        if ($random_int_length > 0) {
            $random_int = rand(pow(10, $random_int_length - 1), pow(10, $random_int_length) - 1);
        }

        return $prefix .
            sprintf("%05d", $digit) .
            $random_int;
    }
}
