<?php

namespace App\Actions;

use App\Exceptions\HiddenValidationException;
use Illuminate\Support\Facades\Validator;

class ValidateHiddenField {
    public function execute(array $rules, array $messages = [])
    {
        $validator = Validator::make(request()->all(), $rules, $messages);

        if ($validator->fails()) {
            throw new HiddenValidationException($validator->errors()->toArray());
        }
    }
}