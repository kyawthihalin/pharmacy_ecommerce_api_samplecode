<?php

namespace App\Services;

use App\Exceptions\ExpiredAppOtpException;
use App\Exceptions\InvalidAppOtpException;
use App\Interfaces\SmsServiceInterface;
use App\Jobs\SendSms;
use App\Models\AppOtp;
use Illuminate\Support\Facades\Hash;

class AppOtpService
{
    public function __construct(private string $phone_number)
    {
    }

    public function send()
    {
        if (!config('app.app_otp')) {
            return;
        }

        //delete all otps for this phone number
        AppOtp::where('phone_number', $this->phone_number)->delete();

        //generate token
        $otp = get_random_digit(6);

        //create otp for a given expired time
        AppOtp::create([
            'phone_number' => $this->phone_number,
            'otp' => $otp,
            'expired_at' => null,
        ]);

    }

    public function verify(string $otp)
    {
        if (!config('app.app_otp')) {
            if ($otp != "000000") {
                throw new InvalidAppOtpException();
            }
            return;
        }

        $app_otp = AppOtp::where('phone_number', $this->phone_number)->first();

        if (!$app_otp) {
            throw new InvalidAppOtpException();
        }

        if ($otp != $app_otp->otp) {
            throw new InvalidAppOtpException();
        }

        $app_otp->delete();
    }
}
