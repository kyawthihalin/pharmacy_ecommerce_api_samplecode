<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class StorageService
{
    public static function getUrl(string $path, int $minutes = 5): string
    {
        if (config('filesystems.default') === 'do' || config('filesystems.default') === 's3') {
            return Storage::temporaryUrl($path, now()->addMinutes($minutes));
        }

        return Storage::url($path);
    }
}
