<?php

namespace App\Services\Sms;

use App\Interfaces\SmsServiceInterface;
use Illuminate\Support\Facades\Http;

class BoomSmsService implements SmsServiceInterface
{
    public function send(string $to, string $message): void
    {
        Http::withHeaders([
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . config('services.boomsms.token')
        ])->withOptions([
            "verify" => app()->environment('production') ?? false
        ])->post(config('services.boomsms.endpoint'), [
            'from' => 'BPSMS MM',
            'text' => $message,
            'to'   => $to
        ]);
    }
}
