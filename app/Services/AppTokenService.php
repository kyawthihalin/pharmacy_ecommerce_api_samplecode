<?php

namespace App\Services;

use App\Exceptions\BadRequestException;
use App\Exceptions\ExpiredAppTokenException;
use App\Models\AppToken;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AppTokenService
{
    public function __construct(private mixed $phone_number)
    {
    }

    public function generate()
    {
        $random_str = Str::random(rand(100, 120));
        AppToken::where('phone_number', $this->phone_number)->delete();

        AppToken::create([
            'phone_number' => $this->phone_number,
            'token' => bcrypt($random_str),
        ]);

        return $random_str;
    }

    public function verify(mixed $token)
    {
        if (!$this->phone_number || !$token) {
            throw new BadRequestException();
        }

        $app_token = AppToken::wherePhoneNumber($this->phone_number)->latest()->first();
        if (!$app_token) {
            throw new BadRequestException();
        }
        
        if (!Hash::check($token, $app_token->token)) {
            throw new BadRequestException();
        }

        if (now()->diffInMinutes($app_token->created_at) >= config('app.expires.app_token')) {
            $token->delete();
            throw new ExpiredAppTokenException();
        }
    }

    public function delete()
    {
        AppToken::where('phone_number', $this->phone_number)->delete();
    }
}
