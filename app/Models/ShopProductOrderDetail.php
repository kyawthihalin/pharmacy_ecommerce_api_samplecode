<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopProductOrderDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
}
