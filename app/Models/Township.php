<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Township extends Model
{
    use HasFactory, Uuid;

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
