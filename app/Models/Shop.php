<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Shop extends Authenticatable
{
    use HasApiTokens, Notifiable, Uuid;

    protected $guarded = ['id'];

    protected $casts = ['freeze_status'];

    public function product_enquires()
    {
        return $this->hasMany(ProductEnquiry::class, 'shop_id');
    }

    public function shop_coupons()
    {
        return $this->hasMany(ShopCoupon::class, 'shop_id');
    }

    public function shipping_addresses()
    {
        return $this->hasMany(ShippingAddress::class, 'shop_id');
    }

    public function cart()
    {
        return $this->hasOne(Cart::class, 'shop_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'shop_id');
    }

    public function product_returns()
    {
        return $this->hasMany(ProductReturn::class, 'shop_id');
    }

    public function stocks()
    {
        return $this->hasMany(ShopStock::class, 'shop_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'shop_id');
    }
}
