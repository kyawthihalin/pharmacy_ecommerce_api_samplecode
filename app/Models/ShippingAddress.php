<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ShippingAddress extends Model
{
    use HasFactory, Uuid;

    protected $guarded = ['id'];

    protected $table = "shipping_addresses";
    protected $casts = [
        'default_shipping_address' => 'boolean'
    ];

    public function township()
    {
        return $this->belongsTo(Township::class, 'township_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->uuid)) {
                $model->uuid = Str::uuid()->toString();
            }
        });

        static::addGlobalScope('shop', function (Builder $builder) {
            $builder->where('shop_id', auth()->id());
        });
    }
}
