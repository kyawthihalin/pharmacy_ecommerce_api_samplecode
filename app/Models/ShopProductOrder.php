<?php

namespace App\Models;

use App\Models\ShopProductOrderDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ShopProductOrder extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function shop_product_order_detail()
    {
        return $this->hasMany(ShopProductOrderDetail::class,'order_id','id');
    }
}
