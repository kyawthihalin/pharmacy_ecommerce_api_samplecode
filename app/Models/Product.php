<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\SkuCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $casts = ['gallery' => 'array'];

    public function getRouteKeyName()
    {
        return 'product_id';
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function order_details()
    {
        return $this->hasMany(OrderDetail::class, 'product_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories');
    }

    public function skuCode()
    {
        return $this->belongsTo(SkuCode::class);
    }

    public function getDiscountAttribute()
    {
        if(!$this->skuCode){
            return 0;
        }

        if ($this->skuCode->discount_type === 'No Discount') {
            return 0;
        }

        if (!$this->skuCode->discount_start_date && !$this->skuCode->discount_end_date) {
            return 0;
        }

        if ($this->skuCode->discount_start_date && !$this->skuCode->discount_end_date) {
            return !Carbon::parse($this->skuCode->discount_start_date)->isFuture() ? $this->skuCode->discount : 0;
        }

        if (!$this->skuCode->discount_start_date && $this->skuCode->discount_end_date) {
            return Carbon::parse($this->skuCode->discount_end_date)->isFuture() ? $this->skuCode->discount : 0;
        }

        return now()->between(
            $this->skuCode->discount_start_date,
            $this->skuCode->discount_end_date
        ) ? $this->skuCode->discount : 0;
    }

    public function getDiscountTypeAttribute()
    {
        if(!$this->skuCode){
            return null;
        }

        return $this->skuCode->discount_type;
    }

    public function getCalculatedDiscountAttribute()
    {
        $discount_amount = $this->getDiscountAttribute();

        if (!$discount_amount) {
            return 0;
        }

        if ($this->getDiscountTypeAttribute() === 'Fixed') {
            return $discount_amount;
        }

        if ($this->getDiscountTypeAttribute() === 'Percent') {
            return (float) bcmul((float)bcdiv($discount_amount, 100, 4), $this->attributes['price']);
        }

        return 0;
    }
}
