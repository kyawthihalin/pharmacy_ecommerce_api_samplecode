<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class AppOtp extends Model
{
    use HasUuids;

    protected $fillable = ['phone_number', 'otp'];
}
