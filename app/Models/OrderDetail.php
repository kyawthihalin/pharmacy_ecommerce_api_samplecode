<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductBalance;

class OrderDetail extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public static function boot() {

        parent::boot();

        static::saving(function($item) {

	        $product_balance = ProductBalance::where('product_id',$item->product_id)
	        				->latest('created_at')
	        				->first();

	        if($product_balance)
	        {
	            $item->purchase_price = $product_balance->price;
	        }
	        else{
	            $item->purchase_price = 0;
	        }
        });
    }
}
