<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopProductBalance extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'shop_product_balances';

    public function products()
    {
        return $this->hasMany(Product::class,'id','product_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
