<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopStock extends Model
{
    use Uuid;
    protected $guarded = ['uuid'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
