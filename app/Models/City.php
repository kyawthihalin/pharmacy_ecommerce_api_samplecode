<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory, Uuid;

    public function townships()
    {
        return $this->hasMany(Township::class, 'city_id');
    }
}
