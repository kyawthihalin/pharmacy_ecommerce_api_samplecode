<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use CyberWings\MyanmarPhoneNumber;

class PhoneNumber implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $validator = new MyanmarPhoneNumber();
        if (!$validator->is_valid($value)) {
            $fail('validation.phone_number')->translate();
        }
    }
}
