<?php

namespace App\Http\Resources\Cart;

use App\Http\Resources\ProductDetailResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): JsonResource
    {
        return new ProductDetailResource($this->products);
    }
}
