<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $order_details = $this->order_details;
        $product_names = [];

        foreach($order_details as $detail){
            array_push($product_names, $detail->product->name);
        }

        return [
            'order_id' => $this->order_no,
            'medicines' => implode(',', $product_names),
            'ordered_at' => $this->created_at->diffForHumans(),
        ];
    }
}
