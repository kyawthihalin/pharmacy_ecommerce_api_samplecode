<?php

namespace App\Http\Resources\AppAddress;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TownshipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->uuid,
            'city' => $this->city->name,
            'name' => $this->name,
            'shipping_fee' => $this->shipping_fees,
        ];
    }
}
