<?php

namespace App\Http\Resources;

use App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'table_id' => $this->id,
            'id' => $this->uuid,
            'name' => $this->name,
            'slug' => $this->slug,
            'photo' => StorageService::getUrl($this->photo),
        ];
    }
}
