<?php

namespace App\Http\Resources;

use App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->name,
            'logo' => StorageService::getUrl($this->shop_logo, config('services.passport.personal_access_token.expires_in') * 60),
            'address' => $this->address,
            'phone' => $this->phone,
            'description' => $this->description,
        ];
    }
}
