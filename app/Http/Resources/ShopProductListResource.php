<?php

namespace App\Http\Resources;

use App\Models\Unit;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Models\ShopProductOrder;
use App\Services\StorageService;
use App\Models\ShopProductBalance;
use App\Models\ShopProductSalePrice;
use App\Models\ShopProductOrderDetail;
use App\Models\ShopProductTotalBalance;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopProductListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $brand = Brand::where('id',$this->brand_id)->first();
        $unit = Unit::where('id',$this->unit_id)->first();
        $unitName = $unit ? $unit->name : '';

        return [
            'id' => $this->id,
            'product_code' => $this->product_id,
            'name' => $this->name,
            'feature_image' => StorageService::getUrl($this->feature_image),
            'brand_name' => $brand ? $brand->name : null, 
            'size' => "{$this->size} {$unitName}", 
            'sale_price' => $this->sale_price ?? 0,
            'total_quantity' => (int) $this->total_quantity,
            'used_quantity' => (int) $this->used_quantity,
            'remain_quantity' => (int) $this->total_quantity - (int) $this->used_quantity,
            'low_stock_warning_quantity' => $this->low_stock_warning_quantity ?? 0,
        ];
    }
}
