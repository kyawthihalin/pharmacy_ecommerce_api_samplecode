<?php

namespace App\Http\Resources;

use App\Models\Shop;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Services\StorageService;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $productBalance = ProductBalance::where('product_id',$this->id)->sum('quantity');

        $shop = Shop::find(auth()->id());

        $available_quantity = $this->limit;

        $cart_product = $shop->cart ? $shop->cart->products()->whereProductId($this->id)->first() : null;

        if ($cart_product && $this->limit) {
            $available_quantity -= $cart_product->quantity;
        }

        return [
            'table_id' => $this->id,
            'id' => $this->product_id,
            'name' => $this->name,
            'price' => number_format($this->price),
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'feature_image' => StorageService::getUrl($this->feature_image),
            'brand_name' => $this->brand->name,
            'size' => "{$this->size} {$this->unit->name}",
            'limit' => $this->limit,
            'available_quantity' => $available_quantity,
            'stock_quantity' => isset($productBalance) ? (int) $productBalance : 0,
            'quantity' => $cart_product ? $cart_product->quantity : 0
        ];
    }
}
