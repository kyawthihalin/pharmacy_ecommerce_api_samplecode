<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShippingAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $township = $this->township;
        $city = $this->city;
        return [
            'id' => $this->uuid,
            'name' => $this->name,
            'phone_number' => $this->phone_number,
            'ship_address' => $this->ship_address,
            'default' => $this->default_shipping_address,
            'shipping_fee' => $township->shipping_fees,
            'city' => [
                'id' => $city->uuid,
                'name' => $city->name,
            ],
            'township' => [
                'id' => $township->uuid,
                'name' => $township->name,
            ],
        ];
    }
}
