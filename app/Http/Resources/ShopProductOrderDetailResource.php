<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ShopProductTotalBalance;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopProductOrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'table_id' => $this->product_id,
            'name' => Product::where('id',$this->product_id)->first()->name,
            'quantity' => $this->quantity,
            'sale_price' => $this->sale_price,
        ];
    }
}
