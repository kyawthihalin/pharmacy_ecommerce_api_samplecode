<?php

namespace App\Http\Resources;

use App\Models\Shop;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Services\StorageService;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $gallery_urls = [];
        if (count($this->gallery)) {
            foreach ($this->gallery as $path) {
                array_push($gallery_urls, StorageService::getUrl($path));
            }
        }

        $shop = Shop::find(auth()->id());

        $available_quantity = $this->limit;

        $product = null;

        if ($request->from_order_detail) {
            $product = OrderDetail::where('product_id', $this->id)->where('order_id', $request->order_id)->first();
        } else {
            $product = $shop?->cart?->products()->whereProductId($this->id)->first();
        }

        if ($product && $this->limit) {
            $available_quantity -= $product->quantity;
        }

        $productBalance = ProductBalance::where('product_id',$this->id)->sum('quantity');

        return [
            'id' => $this->product_id,
            'name' => $this->name,
            'price' => $this->price,
            'discount' => $this->discount,
            'feature_image' => StorageService::getUrl($this->feature_image),
            'brand_name' => $this->brand->name,
            'discount_type' => $this->discount_type,
            'size' => $this->size,
            'size_unit' => $this->unit->name,
            'description' => $this->description,
            'gallery' => $gallery_urls,
            'limit' => $this->limit,
            'available_quantity' => $available_quantity,
            'stock_quantity' =>  isset($productBalance) ? (int) $productBalance : 0,
            'quantity' => $product ? $product->quantity : 0
        ];
    }
}
