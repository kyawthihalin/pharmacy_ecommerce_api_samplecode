<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use App\Http\Resources\ShopProductListResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'invoice_id' => $this->invoice_id,
            'sub_total' => $this->sub_total,
            'tax' => $this->tax,
            'total' => $this->total,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'products' => ShopProductOrderDetailResource::collection($this->shop_product_order_detail)
        ];
    }
}
