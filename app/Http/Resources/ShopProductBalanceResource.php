<?php

namespace App\Http\Resources;

use App\Models\Unit;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Services\StorageService;
use App\Models\ShopProductTotalBalance;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopProductBalanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $brand = Brand::where('id',$this->brand_id)->first();
        $unit = Unit::where('id',$this->unit_id)->first();
        $unitName = $unit ? $unit->name : '';

        $shopProductTotalBalance = ShopProductTotalBalance::where('shop_id',auth()->user()->id)
                                    ->where('product_id',$this->id)->first();

        return [
            'transaction_id' => $this->transaction_id,
            'id' => $this->id,
            'product_code' => $this->product_id,
            'name' => $this->name,
            'feature_image' => StorageService::getUrl($this->feature_image),
            'brand_name' => $brand ? $brand->name : null, 
            'size' => "{$this->size} {$unitName}", 
            'total_quantity' => (int) $this->quantity,
            'used_quantity' => (int) $this->used_quantity,
            'remain_quantity' => (int) $this->quantity - (int) $this->used_quantity,
            'purchase_price' => $this->purchase_price,
            'sale_price' => $shopProductTotalBalance ? $shopProductTotalBalance->sale_price : 0,
            'low_stock_warning_quantity' => $shopProductTotalBalance ? $shopProductTotalBalance->low_stock_warning_quantity : 0,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
