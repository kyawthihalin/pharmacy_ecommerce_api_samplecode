<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @return array<string, mixed>
   */
  public function toArray(Request $request): array
  {
    $product = $this->product;
    return [
      'quantity' => $this->quantity,
      'price' => (int) bcdiv($this->amount, $this->quantity, 2),
      'original_price' => (int) bcdiv($this->amount, $this->quantity, 2),
      'table_id' => $product->id,
      'id' => $product->product_id,
      'name' => $product->name,
    ];
  }
}
