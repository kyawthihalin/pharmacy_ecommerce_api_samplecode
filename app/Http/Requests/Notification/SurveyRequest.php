<?php

namespace App\Http\Requests\Notification;

use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'notification_id' => ['required', 'integer', 'min:1'],
            'answer' => ['required', 'string', 'in:Good,Bad'],
            'reason' => ['nullable', 'string', 'max:500']
        ]);

        return [
            //
        ];
    }
}
