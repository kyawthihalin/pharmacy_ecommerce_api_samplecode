<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ShopProductOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'orders' => ['required', 'array'],
            'orders.*.products' => ['required', 'array'],
            'orders.*.sub_total' => ['required','integer'],
            'orders.*.tax' => ['required','integer'],
            'orders.*.total' => ['required','integer'],
            'orders.*.products.*.product_id' => [
                'required', 'string', Rule::exists('products','id'),
                Rule::exists('shop_product_balances', 'product_id')->where(function ($query) {
                    $query->where('shop_id', auth()->user()->id);
                }),
            ],
            'orders.*.products.*.quantity' => ['required', 'integer', 'min:1'],
            'orders.*.products.*.sale_price' => ['required', 'integer', 'min:1'],
        ];
    }
}
