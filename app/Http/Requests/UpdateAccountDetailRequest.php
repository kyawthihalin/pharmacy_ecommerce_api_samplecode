<?php

namespace App\Http\Requests;

use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'logo' => ['image', 'mimes:png,jpeg,jpg,jfif', 'max:5000'],
            'address' => ['string', 'max:500'],
        ];
    }
}
