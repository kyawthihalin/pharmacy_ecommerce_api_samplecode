<?php

namespace App\Http\Requests\ShippingAddress;

use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class DefaultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'address_id' => ['required', 'uuid'],
        ]);

        return [
            //
        ];
    }
}
