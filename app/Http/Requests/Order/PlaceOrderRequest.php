<?php

namespace App\Http\Requests\Order;

use Illuminate\Validation\Rule;
use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class PlaceOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'screenshot' => [
                'image', 
                'mimes:png,jpeg,jpg,jfif'
            ],
            'multiple_screenshot' => [
                'array',
                'max:10'
            ],
            'multiple_screenshot.*' => [
                'image',
                'mimes:png,jpeg,jpg,jfif',
                'max:10000'
            ],
            'payment_type' => [
                'required_if:payment_type_status,cash_down',
                'uuid',
            ],
            'payment_type_status' => [ Rule::in(['cash_down', 'cash_on_delivery']) ],

        ]);
        
        return [
        ];
    }
}
