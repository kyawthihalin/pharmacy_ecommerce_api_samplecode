<?php

namespace App\Http\Requests\Stock;

use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'product_id' => ['required', 'string'],
            'quantity' => ['required', 'integer', 'min:0'],
        ]);
        
        return [
            //
        ];
    }
}
