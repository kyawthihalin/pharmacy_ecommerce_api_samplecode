<?php

namespace App\Http\Requests\Cart;

use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class SaveCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'products' => ['required', 'array'],
            'products.*.product_id' => ['required', 'string'],
            'products.*.quantity' => ['required', 'integer', 'min:1'],
        ]);
        return [];
    }
}
