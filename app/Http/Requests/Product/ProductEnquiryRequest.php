<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductEnquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'medicine_name' => ['required', 'string', 'max:50'],
            'photo' => ['required', 'image', 'mimes:jpg,jpeg,png,jfif', 'max:5000'],
            'type' => ['required', 'string', 'max:50'],
            'size' => ['required', 'string', 'max:50'],
        ];
    }
}
