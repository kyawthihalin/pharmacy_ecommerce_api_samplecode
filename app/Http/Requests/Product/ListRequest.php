<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'per_page' => ['nullable', 'numeric', 'integer'],
            'products' => ['nullable', 'array'],
            'products.*' => ['string'],
            'categories' => ['nullable'],
            'categories.*' => ['uuid'],
            'brands' => ['nullable'],
            'brands.*' => ['uuid'],
            'search' => ['nullable', 'string', 'max:255'],
        ];
    }
}
