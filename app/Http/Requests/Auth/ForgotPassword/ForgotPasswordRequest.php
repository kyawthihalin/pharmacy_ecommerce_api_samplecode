<?php

namespace App\Http\Requests\Auth\ForgotPassword;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'phone_number' => ['required', 'string', 'regex:/^09\d+$/', new PhoneNumber]
        ];
    }

    public function messages()
    {
        return [
            'phone_number.regex' => __("validation.phone_number"),
        ];
    }
}
