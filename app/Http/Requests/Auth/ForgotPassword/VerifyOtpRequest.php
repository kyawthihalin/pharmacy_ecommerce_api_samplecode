<?php

namespace App\Http\Requests\Auth\ForgotPassword;

use App\Actions\ValidateHiddenField;
use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class VerifyOtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'phone_number' => ['required', 'string', new PhoneNumber],
        ]);
        
        return [
            'otp' => ['required', 'string', 'digits:6'],
        ];
    }
}
