<?php

namespace App\Http\Requests\Auth;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class GetOtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'shop_name' => ['required', 'string', 'max:50'],
            'phone_number' => ['required', 'string', 'unique:shops,phone', new PhoneNumber],
            'password' => ['required', 'string', 'min:6'],
        ];
    }

    public function messages()
    {
        return [
            'phone_number.regex' => __("validation.phone_number"),
            'phone_number.unique' => __("auth.failed.shop_exist")
        ];
    }
}
