<?php

namespace App\Http\Requests\Auth;

use App\Actions\ValidateHiddenField;
use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class VerifyOtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'phone_number' => ['required', 'string', new PhoneNumber, 'unique:shops,phone'],
            'shop_name' => ['required', 'string', 'max:50'],
            // 'address' => ['required', 'string', 'max:500'],
            'password' => ['required', 'string', 'min:6'],
            'firebase_token' => ['nullable', 'string', 'max:255'],
            'logo' => ['image', 'mimes:png,jpeg,jpg,jfif', 'max:5000'],
        ]);

        return [
            'otp' => ['required', 'string', 'digits:6']
        ];
    }
}
