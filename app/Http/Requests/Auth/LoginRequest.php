<?php

namespace App\Http\Requests\Auth;

use App\Actions\ValidateHiddenField;
use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'firebase_token' => ['nullable', 'string', 'max:255'],
        ]);

        return [
            'phone_number' => ['required', 'string', new PhoneNumber],
            'password' => ['required', 'string']
        ];
    }

    public function messages()
    {
        return [
            'phone_number.regex' => __("validation.phone_number"),
        ];
    }
}
