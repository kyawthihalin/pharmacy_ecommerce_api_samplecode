<?php

namespace App\Http\Requests\ProductBalance;

use Illuminate\Validation\Rule;
use App\Actions\ValidateHiddenField;
use Illuminate\Foundation\Http\FormRequest;

class AddProductBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        (new ValidateHiddenField())->execute([
            'products' => ['required', 'array'],
            'products.*.product_id' => ['required', 'string', Rule::exists('products','id')],
            'products.*.purchase_price' => ['required', 'integer', 'min:1'],
        ], [
            'products.*.product_id.required' => 'Product id is required.',
            'products.*.product_id.string' => 'Product id must be a string.',
            'products.*.purchase_price.required' => 'Purchased price is required.',
            'products.*.purchase_price.integer' => 'Purchased price must be an integer.',
            'products.*.purchase_price.min' => 'Purchased price must be at least 1.',
        ]);

        return [
            'products.*.quantity' => ['required', 'integer', 'min:1'],
            'products.*.sale_price' => ['required', 'integer', 'min:1'],
            'products.*.expiry_date' => ['nullable', 'date', 'date_format:d-m-Y'],
            'products.*.low_stock_warning_quantity' => ['nullable', 'integer', 'min:0'],
        ];
    }

    public function messages()
    {
        return [
            'products.*.quantity.required' => 'Quantity is required.',
            'products.*.quantity.integer' => 'Quantity must be an integer.',
            'products.*.quantity.min' => 'Quantity must be at least 1.',
            'products.*.sale_price.required' => 'Sale price is required.',
            'products.*.sale_price.integer' => 'Sale price must be an integer.',
            'products.*.sale_price.min' => 'Sale price must be at least 1.',
            'products.*.low_stock_quantity.integer' => 'Low stock warning must be an integer.',
            'products.*.low_stock_quantity.min' => 'Low stock warning must be at least 0.',
            'products.*.expiry_date.date' => 'Expiry date must be a valid date.',
            'products.*.expiry_date.date_format' => 'Expiry date must be in d-m-Y format.',
        ];
    }
}
