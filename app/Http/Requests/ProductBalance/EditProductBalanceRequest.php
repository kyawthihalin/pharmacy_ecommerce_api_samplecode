<?php

namespace App\Http\Requests\ProductBalance;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EditProductBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'transaction_id' => [
                'required',
                Rule::exists('shop_product_balances','transaction_id')
            ],
            'product_id' => [
                'required', 'string', Rule::exists('products','id'),
            ],
            'quantity' => ['required', 'integer', 'min:1'],
            'purchase_price' => ['required', 'integer', 'min:1'],
            'sale_price' => ['required', 'integer', 'min:1']
        ];
    }
}
