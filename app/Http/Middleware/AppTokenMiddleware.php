<?php

namespace App\Http\Middleware;

use App\Models\AppToken;
use App\Services\AppTokenService;
use App\Traits\ApiResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AppTokenMiddleware
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        (new AppTokenService(phone_number: $request->phone_number))->verify(token: $request->token);
        return $next($request);
    }
}
