<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\ShopResource;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function login(LoginRequest $request)
    {
        $shop = Shop::wherePhone($request->phone_number)->first();

        if (!$shop || !Hash::check($request->password, $shop->password)) {
            return $this->responseWithError(message: "auth.failed.credential", code: 401);
        }

        if ($shop->freeze_status) {
            return $this->responseWithError(message: "auth.failed.frozen", code: 401);
        }

        if (! $shop->is_approve) {
            return $this->responseWithError(message: "auth.failed.approve", code: 401);
        }

        $shop->tokens()->delete();
        $token = $shop->createToken('user')->accessToken;
        $shop->update([
            'token' => $request->firebase_token
        ]);

        return $this->responseWithSuccess([
            'token' => $token,
            'shop' => new ShopResource($shop)
        ]);
    }

    /**
     * Handle the incoming request.
     */
    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        //remove firebase token
        $request->user()->update([
            'token' => null,
        ]);

        return $this->responseWithSuccess();
    }
}
