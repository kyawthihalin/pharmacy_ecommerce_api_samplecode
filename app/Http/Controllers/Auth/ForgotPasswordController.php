<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\BadRequestException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPassword\ForgotPasswordRequest;
use App\Http\Requests\Auth\ForgotPassword\UpdatePasswordRequest;
use App\Http\Requests\Auth\ForgotPassword\VerifyOtpRequest;
use App\Models\Shop;
use App\Services\AppOtpService;
use App\Services\AppTokenService;

class ForgotPasswordController extends Controller
{
    public function forgot(ForgotPasswordRequest $request)
    {
        $shop = Shop::wherePhone($request->phone_number)->first();

        if (!$shop) {
            return $this->responseWithSuccess([
                'token' => (new AppTokenService(phone_number: $request->phone_number))->generate(),
            ]);
        }

        (new AppOtpService(phone_number: $request->phone_number))->send();

        return $this->responseWithSuccess([
            'token' => (new AppTokenService(phone_number: $request->phone_number))->generate(),
        ]);
    }

    public function verifyOtp(VerifyOtpRequest $request)
    {
        (new AppOtpService(phone_number: $request->phone_number))->verify(otp: $request->otp);
        (new AppTokenService(phone_number: $request->phone_number))->delete();

        return $this->responseWithSuccess([
            'token' => (new AppTokenService(phone_number: $request->phone_number))->generate(),
        ]);
    }

    public function update(UpdatePasswordRequest $request)
    {
        $shop = Shop::wherePhone($request->phone_number)->first();

        if (!$shop) {
            throw new BadRequestException();
        }

        (new AppTokenService(phone_number: $request->phone_number))->delete();

        $shop->update([
            'password' => bcrypt($request->password)
        ]);

        return $this->responseWithSuccess();
    }
}
