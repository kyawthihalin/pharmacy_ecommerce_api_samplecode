<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\GetOtpRequest;
use App\Http\Requests\Auth\VerifyOtpRequest;
use App\Http\Resources\ShopResource;
use App\Models\Shop;
use App\Services\AppOtpService;
use App\Services\AppTokenService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    public function getOtp(GetOtpRequest $request)
    {
        // (new AppOtpService(phone_number: $request->phone_number))->send();

        $token = (new AppTokenService(phone_number: $request->phone_number))->generate();

        return $this->responseWithSuccess([
            'token' => $token,
            'is_otp_enable' => false
        ]);
    }

    public function verifyOtp(VerifyOtpRequest $request)
    {
        // (new AppOtpService(phone_number: $request->phone_number))->verify(otp: $request->otp);
        (new AppTokenService(phone_number: $request->phone_number))->delete();

        $created_shop = Shop::create([
            'phone' => $request->phone_number,
            'name' => $request->shop_name,
            'token' => $request->firebase_token,
            'password' => bcrypt($request->password),
            'shop_logo' => 'shops/logo/shop_default.png',
            'is_approve' => 0,
        ]);

        $created_shop->refresh();

        if($request->has('logo')){
            $file = $request->file('logo');
            $path = 'shops/logo/' . time() . '.' . $file->getClientOriginalExtension();
            Storage::put($path, file_get_contents($file));

            $created_shop->update([
                'shop_logo' => $path,
            ]);
        }

        $created_shop->cart()->create();

        $token = $created_shop->createToken('user')->accessToken;
        return $this->responseWithSuccess([
            'token' => $token,
            'shop' => new ShopResource($created_shop),
        ]);
    }
}
