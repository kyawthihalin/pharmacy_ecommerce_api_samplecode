<?php

namespace App\Http\Controllers;

use App\Actions\GetCartList;
use App\Actions\ValidateCoupon;
use App\Exceptions\BadRequestException;
use App\Http\Requests\Cart\CheckCouponRequest;
use App\Http\Requests\Cart\ProductQuantityRequest;
use App\Http\Requests\Cart\RemoveFromCartRequest;
use App\Http\Requests\IncreaseCartRequest;
use App\Http\Requests\Product\AddToCartRequest;
use App\Models\Product;
use App\Models\Shop;

class CartController extends Controller
{
    public function list()
    {
        $cart = Shop::find(auth()->id())->cart;

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }

    public function count()
    {
        $cart = Shop::find(auth()->id())->cart;

        return $this->responseWithSuccess([
            'cart_count' => count($cart->products)
        ]);
    }

    public function addToCart(AddToCartRequest $request)
    {
        $cart = Shop::find(auth()->id())->cart;
        $product = Product::whereProductId($request->product_id)->first();

        if (!$product) {
            throw new BadRequestException();
        }

        $total_quantity = $request->quantity;
        $cart_product = $cart->products()->whereProductId($product->id)->first();

        if ($cart_product) {
            $total_quantity += $cart_product->quantity;
        }

        if ($total_quantity > $product->limit && $product->limit) {
            return $this->responseWithError(message: "cart.limit_reached", code: 422);
        }

        if ($cart_product) {
            $cart_product->update([
                'quantity' => $total_quantity,
            ]);
        } else {
            $cart->products()->create([
                'product_id' => $product->id,
                'quantity' => $total_quantity
            ]);
        }

        return $this->responseWithSuccess();
    }

    public function quantity(ProductQuantityRequest $request)
    {
        $cart = Shop::find(auth()->id())->cart;
        $product = Product::whereProductId($request->product_id)->first();

        if (!$product) {
            throw new BadRequestException();
        }

        $cart_product = $cart->products()->whereProductId($product->id)->first();

        if (!$cart_product) {
            throw new BadRequestException();
        }

        if ($request->quantity > $product->limit && $product->limit) {
            $cart_product->update([
                'quantity' => $product->limit,
            ]);
            
            return $this->responseWithValidationErrors(errors: [
                'product_id' => $product->product_id,
                'quantity' => __("cart.limit_reached"),
                ...(new GetCartList())->execute($cart->products, $cart->coupon_number),
            ], message: "cart.limit_reached");
        }

        $cart_product->update([
            'quantity' => $request->quantity,
        ]);

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }

    public function increase(IncreaseCartRequest $request)
    {
        $cart = Shop::find(auth()->id())->cart;
        $product = Product::whereProductId($request->product_id)->first();

        if (!$product) {
            throw new BadRequestException();
        }

        $cart_product = $cart->products()->whereProductId($product->id)->first();

        if (!$cart_product) {
            throw new BadRequestException();
        }

        $total_quantity = $cart_product->quantity + 1;

        if ($total_quantity > $product->limit && $product->limit) {
            return $this->responseWithValidationErrors(errors: [
                'product_id' => $product->product_id,
                'quantity' => __("cart.limit_reached"),
            ], message: "cart.limit_reached");
        }

        $cart_product->update([
            'quantity' => $total_quantity,
        ]);

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }

    public function decrease(IncreaseCartRequest $request)
    {
        $cart = Shop::find(auth()->id())->cart;
        $product = Product::whereProductId($request->product_id)->first();

        if (!$product) {
            throw new BadRequestException();
        }

        $cart_product = $cart->products()->whereProductId($product->id)->first();
        $total_quantity = $cart_product->quantity;

        if ($cart_product) {
            $total_quantity -= 1;
        }

        if ($total_quantity < 1) {
            return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
            // return $this->responseWithValidationErrors(errors: [
            //     'quantity' => [__("cart.min_product")]
            // ], message: "cart.min_product");
        }

        // if ($total_quantity > $product->limit) {
        //     return $this->responseWithValidationErrors(errors: [
        //         'quantity' => [__("cart.limit_reached")]
        //     ], message: "cart.limit_reached");
        // }

        $cart_product->update([
            'quantity' => $total_quantity,
        ]);

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }

    // public function save(SaveCartRequest $request)
    // {
    //     $shop = Shop::find(auth()->id());

    //     foreach ($request->products as $product_arr) {
    //         $product = Product::whereProductId($product_arr['product_id'])->first();
    //         if (!$product) {
    //             throw new BadRequestException();
    //         }

    //         $total_quantity = $product_arr['quantity'];
    //         $cart_detail = $shop->cart_details()->whereProductId($product->id)->first();

    //         if ($total_quantity > $product->limit) {
    //             return $this->responseWithError(message: "cart.limit_reached", code: 422);
    //         }

    //         $cart_detail->update([
    //             'quantity' => $total_quantity
    //         ]);
    //     }

    //     return $this->responseWithSuccess();
    // }

    public function remove(RemoveFromCartRequest $request)
    {
        $cart = Shop::find(auth()->id())->cart;
        $product = Product::whereProductId($request->product_id)->first();
        if (!$product) {
            throw new BadRequestException();
        }

        $cart_product = $cart->products()->whereProductId($product->id)->first();

        if (!$cart_product) {
            throw new BadRequestException();
        }

        $cart_product->delete();

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }

    public function checkCoupon(CheckCouponRequest $request)
    {
        (new ValidateCoupon(coupon_number: $request->coupon_number))->execute();

        $cart = Shop::find(auth()->id())->cart;

        $cart->update([
            'coupon_number' => $request->coupon_number
        ]);

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }

    public function removeCoupon()
    {
        $cart = Shop::find(auth()->id())->cart;

        $cart->update([
            'coupon_number' => null
        ]);

        return $this->responseWithSuccess((new GetCartList())->execute($cart->products, $cart->coupon_number));
    }
}
