<?php

namespace App\Http\Controllers;

use App\Http\Resources\BrandResource;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function featuredBrands()
    {
        $brands = Brand::whereFeatureStatus(1)->get();
        return $this->responseWithSuccess([
            'brands' => BrandResource::collection($brands),
        ]);
    }

    public function list()
    {
        $brands = Brand::all();

        return $this->responseWithSuccess([
            'brands' => BrandResource::collection($brands),
        ]);
    }
}
