<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function topCategories()
    {
        $categories = Category::whereFeatureStatus(1)->get();
        return $this->responseWithSuccess([
            'categories' => CategoryResource::collection($categories),
        ]);
    }

    public function list()
    {
        $categories = Category::orderBy('name','asc')->get();

        return $this->responseWithSuccess([
            'categories' => CategoryResource::collection($categories),
        ]);
    }
}
