<?php

namespace App\Http\Controllers;

use App\Http\Requests\Stock\AddRequest;
use App\Http\Requests\Stock\DeleteRequest;
use App\Http\Requests\Stock\UpdateRequest;
use App\Http\Resources\Stock\StockListResource;
use App\Models\Product;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    public function list(Request $request)
    {
        $request->validate([
            'per_page' => ['nullable', 'integer']
        ]);
        $shop = Shop::find(auth()->id());

        $stocks = $shop->stocks()->paginate($request->per_page ?? 16);

        return $this->responseWithSuccess([
            'stocks' => StockListResource::collection($stocks),
            'next_pages' => $stocks->nextPageUrl() ?? "",
        ]);
    }

    public function add(AddRequest $request)
    {
        DB::transaction(function () use ($request) {
            foreach ($request->products as $detail) {
                $product = Product::where('product_id', $detail['product_id'])->first();

                if (!$product) {
                    abort(404);
                }

                $shop = Shop::find(auth()->id());
                $shop_product = $shop->stocks()->where('product_id', $product->id)->first();

                if ($shop_product) {
                    $shop_product->update([
                        'quantity' => $shop_product->quantity + $detail['quantity'],
                        'purchased_price' => $detail['purchased_price'],
                        'sale_price' => $detail['sale_price'],
                        'expiry_date' => isset($detail['expiry_date']) ? Carbon::parse($detail['expiry_date'])->format('Y-m-d') : null,
                        'alert_quantity' => isset($detail['low_stock_warning']) ? $detail['low_stock_warning'] : 0,
                    ]);

                    continue;
                }

                $shop->stocks()->create([
                    'product_id' => $product->id,
                    'quantity' => $detail['quantity'],
                    'purchased_price' => $detail['purchased_price'],
                    'sale_price' => $detail['sale_price'],
                    'expiry_date' => isset($detail['expiry_date']) ? Carbon::parse($detail['expiry_date'])->format('Y-m-d') : null,
                    'alert_quantity' => isset($detail['low_stock_warning']) ? $detail['low_stock_warning'] : 0,
                ]);
            }
        });

        return $this->responseWithSuccess();
    }

    public function out(Request $request)
    {
        $request->validate([
            'per_page' => ['nullable', 'integer']
        ]);
        $shop = Shop::find(auth()->id());

        $stocks = $shop->stocks()->where('quantity', 0)->paginate($request->per_page ?? 16);

        return $this->responseWithSuccess([
            'stocks' => StockListResource::collection($stocks),
            'next_pages' => $stocks->nextPageUrl() ?? "",
        ]);
    }

    public function update(UpdateRequest $request)
    {

        $product = Product::where('product_id', $request->product_id)->first();

        if (!$product) {
            abort(404);
        }

        $shop = Shop::find(auth()->id());

        $shop_product = $shop->stocks()->where('product_id', $product->id)->first();

        if ($shop_product) {
            $shop_product->update([
                'quantity' => $request->quantity,
            ]);
        }

        return $this->responseWithSuccess();
    }

    public function destroy(DeleteRequest $request)
    {
        $product = Product::where('product_id', $request->product_id)->first();

        if (!$product) {
            abort(404);
        }

        $shop = Shop::find(auth()->id());

        $shop_product = $shop->stocks()->where('product_id', $product->id)->first();

        if ($shop_product) {
            $shop_product->delete();
        }

        return $this->responseWithSuccess();
    }
}
