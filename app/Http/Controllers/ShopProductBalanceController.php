<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ShopProductBalance;
use App\Models\ShopProductLowStock;
use App\Models\ShopProductSalePrice;
use App\Models\ShopProductTotalBalance;
use Illuminate\Validation\ValidationException;
use App\Http\Resources\ShopProductListResource;
use App\Http\Resources\ShopProductBalanceResource;
use App\Http\Requests\ProductBalance\AddProductBalanceRequest;
use App\Http\Requests\ProductBalance\EditProductBalanceRequest;

class ShopProductBalanceController extends Controller
{   
    public function list(Request $request)
    {
        $perPage = $request->per_page ?? 16;
        $keyword = $request->input('name');

        $totalStockQty = ShopProductBalance::where('shop_id',auth()->user()->id)->sum('quantity');
        $totalPurchasePrice = ShopProductBalance::where('shop_id',auth()->user()->id)->sum('purchase_price');

        $sortDirection = $request->input('sorting', 'desc');

        $data = ShopProductBalance::select('products.*')
                ->selectRaw('shop_product_balances.purchase_price as purchase_price')
                ->selectRaw('shop_product_balances.quantity as quantity')
                ->selectRaw('shop_product_balances.used_quantity as used_quantity')
                ->selectRaw('shop_product_balances.transaction_id as transaction_id')
                ->join('products', 'shop_product_balances.product_id', '=', 'products.id')
                ->where('shop_product_balances.shop_id', auth()->user()->id);

        if ($keyword) {
            $data->where('products.name', 'like', '%' . $keyword . '%');
        }

        if ($sortDirection === 'asc') {
            $data->orderBy('shop_product_balances.id', 'asc');
        } else {
            $data->orderBy('shop_product_balances.id', 'desc');
        }

        $data = $data->paginate($perPage);

        return $this->responseWithSuccess([
            'balances' => ShopProductBalanceResource::collection($data),
            'total_quantity' => $totalStockQty,
            'total_purchase_price' => $totalPurchasePrice,
            'next_pages' => $data->nextPageUrl() ?? "",
        ]);
    }

    public function add(AddProductBalanceRequest $request)
    {   
        foreach($request->products as $detail){

            $product = Product::where('id',$detail['product_id'])->first();

            ShopProductBalance::create([
                'transaction_id' => time() . mt_rand(100, 999),
                'shop_id' => auth()->user()->id,
                'product_id' => $detail['product_id'],
                'categories_id' => $product->categories()->first() ? $product->categories()->first()->id : null,
                'quantity' => $detail['quantity'],
                'purchase_price' => $detail['purchase_price'],
                'description' => isset($detail['description']) ? $detail['description'] : null,
                'expiry_date' => isset($detail['expiry_date']) ? Carbon::parse($detail['expiry_date'])->format('Y-m-d') : null,
            ]); 

            $shopProductTotalBalance =  ShopProductTotalBalance::where('shop_id',auth()->user()->id)
                                                        ->where('product_id',$detail['product_id'])
                                                        ->first();
            if(! $shopProductTotalBalance){
                ShopProductTotalBalance::create([
                    'shop_id' => auth()->user()->id,
                    'product_id' => $detail['product_id'],
                    'categories_id' => $product->categories()->first() ? $product->categories()->first()->id : null,
                    'sale_price' => $detail['sale_price'],
                    'low_stock_warning_quantity' => isset($detail['low_stock_warning_quantity']) ? $detail['low_stock_warning_quantity'] : 0
                ]);
            }else{
                $shopProductTotalBalance->update([
                    'sale_price' => $detail['sale_price'],
                    'low_stock_warning_quantity' => isset($detail['low_stock_warning_quantity']) ? $detail['low_stock_warning_quantity'] : 0
                ]);
            }
        }

        return $this->responseWithSuccess();
    }

    public function edit(EditProductBalanceRequest $request)
    {
        $shopProductBalance = ShopProductBalance::where('transaction_id',$request->transaction_id)
                        ->where('shop_id',auth()->user()->id)
                        ->where('product_id',$request->product_id)
                        ->first();

        if(!$shopProductBalance) {
            throw ValidationException::withMessages(['transaction_id' => 'The selected transaction id is invalid']);
        }

        if($request->quantity < $shopProductBalance->used_quantity){
            throw ValidationException::withMessages(['quantity' => 'The edit quantity cannot be less than used quantity']);
        }

        $shopProductBalance->quantity = $request->quantity;
        $shopProductBalance->purchase_price = $request->purchase_price;
        $shopProductBalance->save();

        $shopProductTotalBalance =  ShopProductTotalBalance::where('shop_id',auth()->user()->id)
                        ->where('product_id',$request->product_id)
                        ->first();
                
        $shopProductTotalBalance->sale_price = $request->sale_price;
        $shopProductTotalBalance->save();

        return $this->responseWithSuccess();

     }
}
