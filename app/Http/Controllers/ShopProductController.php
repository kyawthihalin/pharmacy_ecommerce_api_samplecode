<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\Tax;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProfitMargin;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\ShopProductOrder;
use App\Models\ShopProductBalance;
use Illuminate\Support\Facades\DB;
use App\Models\ShopProductOrderDetail;
use App\Models\ShopProductTotalBalance;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\InvoiceDetailResource;
use App\Http\Requests\ShopProductOrderRequest;
use Illuminate\Validation\ValidationException;
use App\Http\Resources\ShopProductListResource;
use App\Http\Resources\ShopProductOrderResource;

class ShopProductController extends Controller
{
    public function getProduct(Request $request)
    {
        $categoryId = $request->category_id;

        $perPage = $request->per_page ?? 16;
        $keyword = $request->input('name');

        $shopId = auth()->user()->id;

        $query = ShopProductBalance::select(
                    'products.*',
                    \DB::raw('SUM(shop_product_balances.quantity) as total_quantity'),
                    \DB::raw('SUM(shop_product_balances.used_quantity) as used_quantity'),
                    \DB::raw('(SELECT sale_price FROM shop_product_total_balances 
                        WHERE shop_product_total_balances.product_id = products.id 
                        AND shop_product_total_balances.shop_id = ' . $shopId . ' 
                        LIMIT 1) as sale_price'
                    ),
                    \DB::raw('(SELECT low_stock_warning_quantity FROM shop_product_total_balances 
                        WHERE shop_product_total_balances.product_id = products.id 
                        AND shop_product_total_balances.shop_id = ' . $shopId . ' 
                        LIMIT 1) as low_stock_warning_quantity'
                    )
                )
                ->join('products', 'shop_product_balances.product_id', '=', 'products.id')
                ->where('shop_id', $shopId)
                ->when($categoryId, function ($query) use ($categoryId, $shopId) {
                    $query->where('shop_product_balances.categories_id', $categoryId);
                })
                ->when($keyword, function ($query) use ($keyword, $shopId) {
                    $query->where('products.name', 'LIKE', '%' . $keyword . '%');
                })
                ->groupBy('products.id')
                ->havingRaw('SUM(shop_product_balances.quantity) > SUM(shop_product_balances.used_quantity)')
                ->distinct();        
    
        $products = $query->paginate($perPage);

        return $this->responseWithSuccess([
            'products' => ShopProductListResource::collection($products),
            'next_pages' => $products->nextPageUrl() ?? "",
        ]);
    }

    public function getLowStockProduct(Request $request)
    {
        $perPage = $request->per_page ?? 16;
        $keyword = $request->input('name');

        $shopId = auth()->user()->id;

        $query = ShopProductBalance::select(
            'products.*',
                \DB::raw('SUM(shop_product_balances.quantity) as total_quantity'),
                \DB::raw('SUM(shop_product_balances.used_quantity) as used_quantity'),
                \DB::raw('(SELECT sale_price FROM shop_product_total_balances 
                    WHERE shop_product_total_balances.product_id = products.id 
                    AND shop_product_total_balances.shop_id = ' . $shopId . ' 
                    LIMIT 1) as sale_price'
                ),
                \DB::raw('(SELECT low_stock_warning_quantity FROM shop_product_total_balances 
                    WHERE shop_product_total_balances.product_id = products.id 
                    AND shop_product_total_balances.shop_id = ' . $shopId . ' 
                    LIMIT 1) as low_stock_warning_quantity'
                )
            )
            ->join('products', 'shop_product_balances.product_id', '=', 'products.id')
            ->where('shop_id', $shopId)
            ->when($keyword, function ($query) use ($keyword) {
                $query->where('products.name', 'LIKE', '%' . $keyword . '%');
            })
            ->groupBy('products.id')
            ->havingRaw('(total_quantity - used_quantity) <= low_stock_warning_quantity')
            ->distinct();
        
        $products = $query->paginate($perPage);

        return $this->responseWithSuccess([
            'products' => ShopProductListResource::collection($products),
            'next_pages' => $products->nextPageUrl() ?? "",
        ]);
    }

    public function getCategory()
    {
        $shopId = auth()->user()->id;

        $categoriesId = 
            ShopProductBalance::
            select('products.*',
                \DB::raw('SUM(shop_product_balances.quantity) as total_quantity'),
                \DB::raw('SUM(shop_product_balances.used_quantity) as used_quantity'),
                \DB::raw('(SELECT sale_price FROM shop_product_total_balances 
                    WHERE shop_product_total_balances.product_id = products.id 
                    AND shop_product_total_balances.shop_id = ' . $shopId . ' 
                    LIMIT 1) as sale_price'
                ),
                \DB::raw('(SELECT low_stock_warning_quantity FROM shop_product_total_balances 
                    WHERE shop_product_total_balances.product_id = products.id 
                    AND shop_product_total_balances.shop_id = ' . $shopId . ' 
                    LIMIT 1) as low_stock_warning_quantity'
                )
            )
            ->select('product_categories.category_id')
            ->join('products', 'shop_product_balances.product_id', '=', 'products.id')
            ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->havingRaw('SUM(shop_product_balances.quantity) > SUM(shop_product_balances.used_quantity)')
            ->where('shop_id', $shopId)
            ->groupBy('product_categories.category_id')
            ->distinct()
            ->pluck('product_categories.category_id');

        $query = Category::whereIn('id',$categoriesId)->get();

        return $this->responseWithSuccess([
            'categories' => CategoryResource::collection($query),
        ]);
    }

    public function order(ShopProductOrderRequest $request)
    {
        try {
            
            DB::beginTransaction();

            foreach($request->orders as $order){

                $shopProductOrder = ShopProductOrder::create([
                    'invoice_id' =>  time() . mt_rand(100, 999),
                    'shop_id' => auth()->user()->id,
                    'sub_total' => $order['sub_total'],
                    'tax' => $order['tax'],
                    'total' => $order['total']
                ]);

                foreach ($order['products'] as $detail) {
        
                    $remainingQuantity = $detail['quantity']; 
                
                    $shopProductBalances = ShopProductBalance::where('product_id', $detail['product_id'])
                        ->where('shop_id', auth()->user()->id)
                        ->orderBy('created_at')
                        ->get();
                
                    foreach ($shopProductBalances as $balance) {
                        $availableQuantity = $balance->quantity - $balance->used_quantity;
        
                        if ($availableQuantity <= 0) {
                            continue; 
                        }
        
                        $quantityToUse = min($remainingQuantity, $availableQuantity);
        
                        $balance->used_quantity += $quantityToUse;
                        $balance->save();
                
                        $remainingQuantity -= $quantityToUse;
        
                        if ($remainingQuantity <= 0) {
                            break;
                        }
                    }
        
                    if ($remainingQuantity > 0) {
                        break;
                    }
                    
                    ShopProductOrderDetail::create([
                        'order_id' => $shopProductOrder->id,
                        'product_id' => $detail['product_id'],
                        'quantity' => $detail['quantity'],
                        'sale_price' => $detail['sale_price'],
                    ]);
                }
            }
        
            DB::commit();
            return $this->responseWithSuccess();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function invoice(Request $request)
    {
        $sortDirection = $request->input('sorting', 'desc');

        $data = ShopProductOrder::with('shop_product_order_detail')
                ->where('shop_id', auth()->user()->id)
                ->when($request->has('invoice_no'), function ($query) use ($request) {
                    return $query->where('invoice_id', '=', $request->invoice_no);
                })
                ->orderBy('id', $request->has('sorting') && $request->sorting === 'asc' ? 'asc' : 'desc')
                ->paginate($request->per_page ?? 16);
    
        return $this->responseWithSuccess([
            'invoices' => ShopProductOrderResource::collection($data),
            'next_pages' => $data->nextPageUrl() ?? "",
        ]);
    }

    public function invoiceDetail(Request $request)
    {
        $request->validate([
            'invoice_no' => [
                'required',
                Rule::exists('shop_product_orders','invoice_id')
            ]
        ]);
        
        $data = ShopProductOrder::with('shop_product_order_detail')
                ->where('shop_id', auth()->user()->id)
                ->where('invoice_id', '=', $request->invoice_no)
                ->first();
    
        return $this->responseWithSuccess([
            'invoice' =>new InvoiceDetailResource($data)
        ]);
    }

    public function revenue()
    {
        $todayDate = Carbon::today();

        $totalRevenue = ShopProductOrder::where('shop_id',auth()->user()->id)->sum('total');
        $todayRevenue = ShopProductOrder::where('shop_id',auth()->user()->id)
                                            ->whereDate('created_at', $todayDate)
                                            ->sum('total');

        $totalQuantity = ShopProductBalance::where('shop_id',auth()->user()->id)->sum('quantity');
        $usedQuantity = ShopProductBalance::where('shop_id',auth()->user()->id)->sum('used_quantity');
        $remainingQuantity = $totalQuantity - $usedQuantity;

        return $this->responseWithSuccess([
            'total_revenue' => (int) $totalRevenue,
            'today_revenue' => (int) $todayRevenue,
            'product_balance' => $remainingQuantity
        ]);
    }

    public function getTax()
    {
        $tax = Tax::first();

        return $this->responseWithSuccess([
            'tax_percentage' => $tax ? $tax->rate : '2',
        ]);
    }

    public function setProfitMargin(Request $request)
    {
        $request->validate([
            'rate' => 'required|numeric|between:0,99',
        ]);

        $profitMargin = ProfitMargin::where('shop_id',auth()->user()->id)->first();

        if($profitMargin) {
            $profitMargin->rate = $request->rate;
            $profitMargin->save();
        }else{
            $profitMargin = ProfitMargin::create([
                'shop_id' => auth()->user()->id,
                'rate' => $request->rate
            ]);    
        }

        $profitMargin = $profitMargin->refresh();

        return $this->responseWithSuccess([
            'profit_margin' => $profitMargin ? $profitMargin->rate : null,
        ]);

    }

    public function getProfitMargin()
    {
        $profitMargin = ProfitMargin::where('shop_id',auth()->user()->id)->first();

        return $this->responseWithSuccess([
            'profit_margin' => $profitMargin ? $profitMargin->rate : null,
        ]);
    }
}
