<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateAccountDetailRequest;
use App\Http\Resources\ShopResource;
use App\Models\Shop;
use Illuminate\Support\Facades\Storage;

class AccountDetailController extends Controller
{
    public function detail()
    {
        return $this->responseWithSuccess([
            'detail' => new ShopResource(auth()->user())
        ]);
    }

    public function update(UpdateAccountDetailRequest $request)
    {
        $shop = Shop::lockForUpdate()->find(auth()->id());

        $shop->update([
            'name' => $request->name,
            'address' => $request->address,
        ]);
        
        if($request->has('logo')){
            if(Storage::exists($shop->shop_logo) && $shop->shop_logo != 'shops/logo/shop_default.png'){
                Storage::delete($shop->shop_logo);
            }
            $file = $request->file('logo');
            $path = 'shops/logo/' . time() . '.' . $file->getClientOriginalExtension();
            Storage::put($path, file_get_contents($file));

            $shop->update([
                'shop_logo' => $path,
            ]);
        }

        return $this->responseWithSuccess([
            'detail' => new ShopResource($shop)
        ]);
    }
}
