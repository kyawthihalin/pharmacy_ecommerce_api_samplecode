<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Services\StorageService;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function __invoke()
    {
        $banner = Banner::latest()->first();
        if (!$banner) {
            abort(404);
        }
        $photos = [];

        foreach ($banner->photos as $photo) {
            array_push($photos, StorageService::getUrl($photo));
        }

        return $this->responseWithSuccess([
            'banners' => $photos
        ]);
    }
}
