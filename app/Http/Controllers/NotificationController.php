<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notification\ReadRequest;
use App\Http\Requests\Notification\SurveyRequest;
use App\Http\Resources\Notification\NotificationListResource;
use App\Models\Shop;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function list(Request $request)
    {
        $request->validate([
            'per_page' => ['nullable', 'integer']
        ]);
        $shop = Shop::find(auth()->id());

        $notifications = $shop->notifications()->whereNull('answer')->orderBy('created_at', 'desc')->paginate($request->per_page ?? 16);

        return $this->responseWithSuccess([
            'notifications' => NotificationListResource::collection($notifications),
            'next_pages' => $notifications->nextPageUrl() ?? "",
        ]);
    }

    public function unreadCount()
    {
        $shop = Shop::find(auth()->id());

        $count = $shop->notifications()->whereNull('read_at')->count();

        return $this->responseWithSuccess([
            'count' => $count
        ]);
    }

    public function readAll()
    {
        $shop = Shop::find(auth()->id());

        $shop->notifications()->whereNull('read_at')->update([
            'read_at' => now()
        ]);

        return $this->responseWithSuccess();
    }

    public function read(ReadRequest $request)
    {
        $shop = Shop::find(auth()->id());

        $shop->notifications()->where('id', $request->notification_id)->update([
            'read_at' => now()
        ]);

        return $this->responseWithSuccess();
    }

    public function unread(ReadRequest $request)
    {
        $shop = Shop::find(auth()->id());

        $shop->notifications()->where('id', $request->notification_id)->update([
            'read_at' => null
        ]);

        return $this->responseWithSuccess();
    }

    public function survey(SurveyRequest $request)
    {
        $shop = Shop::find(auth()->id());

        $shop->notifications()->where('id', $request->notification_id)->update([
            'read_at' => now(),
            'answer' => $request->answer,
            'reason' => $request->reason ?? null,
        ]);

        return $this->responseWithSuccess();
    }
}
