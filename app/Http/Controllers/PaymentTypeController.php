<?php

namespace App\Http\Controllers;

use App\Http\Resources\PaymentTypeResource;
use App\Models\PaymentType;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    public function __invoke()
    {
        $payment_types = PaymentType::whereStatus(1)->get();

        return $this->responseWithSuccess([
            'payment_types' => PaymentTypeResource::collection($payment_types)
        ]);
    }
}
