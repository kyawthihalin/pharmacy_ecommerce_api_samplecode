<?php

namespace App\Http\Controllers;

use App\Actions\ValidateHiddenField;
use App\Http\Requests\Product\ListRequest;
use App\Http\Requests\Product\ProductEnquiryRequest;
use App\Http\Requests\Product\ReturnRequest;
use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductListResource;
use App\Models\Product;
use App\Models\Shop;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function list(ListRequest $request)
    {
        $products = Product::whereStatus(1);

        if ($request->has('products')) {
            $products->whereIn('product_id', $request->products);
        }

        if ($request->has('categories')) {
            $products->whereHas('categories', function ($query)  use ($request) {
                $query->where('uuid', $request->categories);
            });
        }

        if ($request->has('brands')) {
            $products->whereHas('brand', function ($query)  use ($request) {
                $query->where('uuid', $request->brands);
            });
        }

        if ($request->has('search_alphabet')) {
            if ($request->search_alphabet === "") {
                $products->where('id', '<', 1);
            } else {
                $products->whereRaw("LEFT(name, 1) = ?", [$request->search_alphabet]);
            }
        }

        if ($request->has('search')) {
            if ($request->search === "") {
                $products->where('id', '<', 1);
            } else {
                $products->where('name', 'LIKE', "%{$request->search}%")
                    ->orWhereHas('categories', function ($query)  use ($request) {
                        $query->where('name', 'LIKE', "%{$request->search}%");
                    })
                    ->orWhereHas('brand', function ($query)  use ($request) {
                        $query->where('name', 'LIKE', "%{$request->search}%");
                    });
            }
        }

        $products = $products->where('status',1)->paginate($request->per_page ?? 16);

        return $this->responseWithSuccess([
            'products' => ProductListResource::collection($products),
            'next_pages' => $products->nextPageUrl() ?? "",
        ]);
    }

    public function dealsOfTheWeek()
    {
        $products = Product::whereStatus(1)->orderBy('id','desc')->limit(7)->get();

        return $this->responseWithSuccess([
            'products' => ProductListResource::collection($products),
        ]);
    }

    public function bestSelling()
    {
        $products = Product::whereStatus(1)->where('is_selling',1)->orderBy('id','desc')->get();

        return $this->responseWithSuccess([
            'products' => ProductListResource::collection($products),
        ]);
    }

    public function popular()
    {
        $products = Product::whereStatus(1)->where('is_popular',1)->orderBy('id','desc')->get();

        return $this->responseWithSuccess([
            'products' => ProductListResource::collection($products),
        ]);
    }

    public function detail(Request $request)
    {
        (new ValidateHiddenField())->execute([
            'product_id' => ['required', 'string'],
        ]);

        $product = Product::whereProductId($request->product_id)->first();

        if (!$product || !$product->status) {
            abort(404);
        }

        return $this->responseWithSuccess([
            'products' => new ProductDetailResource($product),
        ]);
    }

    public function request(ProductEnquiryRequest $request)
    {
        try {
            $file = $request->file('photo');
            $path = 'enquires/' . time() . '.' . $file->getClientOriginalExtension();
            Storage::put($path, file_get_contents($file));

            auth()->user()->product_enquires()->create([
                'medicine_name' => $request->medicine_name,
                'photo' => $path,
                'type' => $request->type,
                'size' => $request->size,
            ]);
        } catch (Exception $e) {
            Storage::delete($path);
        }

        return $this->responseWithSuccess();
    }

    public function productReturn(ReturnRequest $request)
    {
        $shop = Shop::find(auth()->id());

        $product = Product::where('product_id', $request->medicine_name)->first();

        $shop->product_returns()->create([
            'product_id' => $product->id,
            'size' => $request->size,
            'price' => $request->price,
            'order_no' => $request->order_no,
            'description' => $request->description,
        ]);

        return $this->responseWithSuccess();
    }
}
