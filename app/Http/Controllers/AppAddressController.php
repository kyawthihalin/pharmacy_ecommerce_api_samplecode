<?php

namespace App\Http\Controllers;

use App\Actions\ValidateHiddenField;
use App\Http\Resources\AppAddress\CityResource;
use App\Http\Resources\AppAddress\TownshipResource;
use App\Models\City;
use Illuminate\Http\Request;

class AppAddressController extends Controller
{
    public function getCities()
    {
        $cities = City::all();

        return $this->responseWithSuccess([
            'cities' => CityResource::collection($cities),
        ]);
    }

    public function getTownships(Request $request)
    {
        (new ValidateHiddenField())->execute([
            'city' => ['required', 'string', 'uuid']
        ]);

        $city = City::whereUuid($request->city)->first();

        if (!$city) {
            abort(404);
        }

        $townships = $city->townships;

        return $this->responseWithSuccess([
            'townships' => TownshipResource::collection($townships),
        ]);
    }
}
