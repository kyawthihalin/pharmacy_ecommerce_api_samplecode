<?php

namespace App\Http\Controllers;

use App\Actions\GetCartList;
use App\Actions\ValidateHiddenField;
use App\Constants\Limit;
use App\Exceptions\BadRequestException;
use App\Http\Requests\ShippingAddress\CreateRequest;
use App\Http\Requests\ShippingAddress\DefaultRequest;
use App\Http\Requests\ShippingAddress\UpdateRequest;
use App\Http\Resources\ShippingAddressResource;
use App\Models\ShippingAddress;
use App\Models\Shop;
use App\Models\Township;
use Illuminate\Http\Request;

class ShippingAddressController extends Controller
{
    public function list()
    {
        $addresses = ShippingAddress::whereShopId(auth()->id())->get();
        $cart = Shop::find(auth()->id())->cart;

        return $this->responseWithSuccess([
            'addresses' => ShippingAddressResource::collection($addresses),
            ...(new GetCartList())->execute($cart->products, $cart->coupon_number),
        ]);
    }

    public function create(CreateRequest $request)
    {
        $shop = Shop::find(auth()->id());

        if ($shop->shipping_addresses()->count() === Limit::SHIPPING_ADDRESS) {
            return $this->responseWithError(message: "global.over_limit.shipping_address", code: 422);
        }

        $township = Township::whereUuid($request->township)->first();

        if (!$township) {
            throw new BadRequestException();
        }

        if ($request->default) {
            ShippingAddress::where('uuid', '!=', $request->address_id)->update([
                'default_shipping_address' => false
            ]);
        }

        $shop->shipping_addresses()->create([
            'city_id' => $township->city->id,
            'township_id' => $township->id,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'ship_address' => $request->shipping_address,
            'default_shipping_address' => $request->default,
        ]);

        return $this->responseWithSuccess();
    }

    public function show(Request $request)
    {
        (new ValidateHiddenField())->execute([
            'shipping_address' => ['required', 'uuid'],
        ]);

        $shipping_address = auth()->user()->shipping_addresses()->whereUuid($request->shipping_address)->first();

        if (!$shipping_address) {
            abort(404);
        }

        return $this->responseWithSuccess([
            'address' => new ShippingAddressResource($shipping_address),
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $shop = Shop::find(auth()->id());

        $shipping_address = $shop->shipping_addresses()->whereUuid($request->address_id)->first();

        if (!$shipping_address) {
            abort(404);
        }

        $township = Township::whereUuid($request->township)->first();

        if (!$township) {
            throw new BadRequestException();
        }

        if ($request->has('default')) {
            ShippingAddress::where('uuid', '!=', $request->address_id)->update([
                'default_shipping_address' => false
            ]);
        }

        $shipping_address->update([
            'city_id' => $township->city->id,
            'township_id' => $township->id,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'ship_address' => $request->shipping_address,
            'default_shipping_address' => $request->has('default') ? $request->default : $shipping_address->default_shipping_address 
        ]);

        return $this->responseWithSuccess();
    }

    public function default(DefaultRequest $request)
    {
        $shop = Shop::find(auth()->id());
        $shipping_address = $shop->shipping_addresses()->whereUuid($request->address_id)->first();

        if (!$shipping_address) {
            abort(404);
        }

        $shop->shipping_addresses()->where('uuid', '!=', $request->address_id)->update([
            'default_shipping_address' => false
        ]);

        $shipping_address->default_shipping_address = true;
        $shipping_address->save();

        $addresses = ShippingAddress::whereShopId(auth()->id())->get();
        $cart = $shop->cart;
        
        return $this->responseWithSuccess([
            'addresses' => ShippingAddressResource::collection($addresses),
            (new GetCartList())->execute($cart->products, $cart->coupon_number),
        ]);
    }
}
