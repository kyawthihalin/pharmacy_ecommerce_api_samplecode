<?php

namespace App\Http\Controllers;

use App\Actions\GenerateReference;
use App\Actions\GetCartList;
use App\Exceptions\BadRequestException;
use App\Http\Requests\Order\PlaceOrderRequest;
use App\Http\Requests\Order\ProductRequest;
use App\Http\Requests\Order\ReplaceOrderRequest;
use App\Http\Resources\OrderProductResource;
use App\Http\Resources\OrderResource;
use App\Jobs\OrderSubmitted;
use App\Models\Order;
use App\Models\PaymentType;
use App\Models\ShippingAddress;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    public function order(PlaceOrderRequest $request)
    {
        $shipping_address = ShippingAddress::where('default_shipping_address', true)->first();
        $payment_type = PaymentType::whereUuid($request->payment_type)->first();

        if (!$shipping_address) {
            throw new BadRequestException();
        }

        $cart = Shop::find(auth()->id())->cart;
        $cart_detail = (new GetCartList())->execute($cart->products, $cart->coupon_number);

        if (!$cart_detail['product_count']) {
            throw new BadRequestException();
        }

        $order = DB::transaction(function () use ($request, $payment_type, $shipping_address, $cart_detail) {
            $shop = Shop::lockForUpdate()->find(auth()->id());

            $order = $shop->orders()->create([
                'shipping_address_id' => $shipping_address->id,
                'order_no' => Str::uuid(),
                'payment_type_id' => $payment_type ? $payment_type->id : null,
                'payment_type_status' => $request->payment_type_status,
                'total_quantity' => $cart_detail['total_product_quantity'],
                'total_amount' => $cart_detail['total_product_amount'],
                'discount_amount' => $cart_detail['total_product_discount'],
                'coupon_discount' => $cart_detail['coupon_discount'],
                'grand_total' => $cart_detail['grand_total'],
                'net_amount' => $cart_detail['net_amount'],
                'shipping_fees' => $cart_detail['shipping_fee'],
                'shipping_status' => 'Pending',
                'payment_status' => 'Pending',
                'phone_model' => $request->phone_model ? $request->phone_model : 'Unknown Device'
            ]);
            
            if($request->has('screenshot')){
                $file = $request->file('screenshot');
                $path = 'orders/' . time() . '.' . $file->getClientOriginalExtension();
                Storage::put($path, file_get_contents($file));
            }

            $storePaths = [];

            if($request->has('multiple_screenshot')){
                foreach($request->multiple_screenshot as $screenShot)
                {
                    $path = 'orders/' . time() . '.' . $screenShot->getClientOriginalExtension();
                    Storage::put($path, file_get_contents($screenShot));
    
                    $storePaths[] = $path;
                }
            }

            $order->refresh();

            $order->update([
                'multiple_payment_screenshot' => json_encode($storePaths),
                'payment_screenshot' => isset($path) ? $path : null,
                'order_no' => (new GenerateReference())->execute(prefix: 'MH', digit: $order->id)
            ]);

            foreach ($shop->cart->products as $cart_product) {
                $product = $cart_product->product;
                $amount = (float) bcmul($cart_product->quantity, $product->price, 4);
                $discount_amount = (float) bcmul($cart_product->quantity, $product->calculated_discount, 4);

                $product->order_details()->create([
                    'order_id' => $order->id,
                    'quantity' => $cart_product->quantity,
                    'amount' => $amount,
                    'discount_amount' => $discount_amount
                ]);
            }

            $shop->cart->products()->delete();

            return $order;
        }, 5);

        OrderSubmitted::dispatch(order: $order);

        return $this->responseWithSuccess(message: "order.submitted");
    }

    public function history(Request $request)
    {
        $request->validate([
            'per_page' => ['nullable', 'integer']
        ]);
        $shop = Shop::lockForUpdate()->find(auth()->id());

        $orders = $shop->orders()->orderBy('created_at', 'desc')->paginate($request->per_page ?? 16);

        return $this->responseWithSuccess([
            'orders' => OrderResource::collection($orders),
            'next_pages' => $orders->nextPageUrl() ?? "",
        ]);
    }

    public function detail(ReplaceOrderRequest $request)
    {
        $shop = Shop::find(auth()->id());
        $order = $shop->orders()->where('order_no', $request->order_id)->first();
        request()->merge([
            'from_order_detail' => true,
            'order_id' => $order->id,
            'shipping_status' => $order->shipping_status,
            'payment_status' => $order->payment_status,
            
        ]);
        $cart_detail = (new GetCartList())->execute($order->order_details, optional($order->coupon)->coupon_number);

        return $this->responseWithSuccess($cart_detail);
    }

    public function replace(ReplaceOrderRequest $request)
    {
        $shop = Shop::lockForUpdate()->find(auth()->id());
        $order = $shop->orders()->where('order_no', $request->order_id)->first();

        if (!$order) {
            throw new BadRequestException();
        }
        $cart = $shop->cart;
        $coupon = $order->coupon;
        $shipping_address = $order->shipping_address;

        $shop->shipping_addresses()->where('uuid', '!=', $shipping_address->uuid)->update([
            'default_shipping_address' => false,
        ]);

        $shipping_address->update([
            'default_shipping_address' => true,
        ]);

        if ($coupon) {
            $cart->update([
                'coupon_number' => $coupon->coupon_number,
            ]);
        }

        $order_detail = $order->order_details;
        $cart->products()->delete();
        foreach ($order_detail as $detail) {
            $cart->products()->create([
                'product_id' => $detail->product_id,
                'quantity' => $detail->quantity,
            ]);
        }

        return $this->responseWithSuccess();
    }

    public function products(ProductRequest $request)
    {
        $order = Order::where('uuid', $request->order_id)->first();

        if (!$order) {
            abort(404);
        }

        $order_details = $order->order_details;
        return $this->responseWithSuccess([
            'products' => OrderProductResource::collection($order_details),
        ]);
    }
}
