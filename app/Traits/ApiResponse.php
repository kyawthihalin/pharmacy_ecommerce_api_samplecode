<?php

namespace App\Traits;

trait ApiResponse
{
    protected function responseWithSuccess($data = [], $message = "global.success", $code = 200)
    {
        $this->setLocale();
        return response()->json([
            ...$data,
            'message' => __($message),
            'code' => $code,
        ], $code);
    }

    protected function responseWithError($message = "global.bad_request", $code = 400)
    {
        $this->setLocale();
        return response()->json([
            'message' => __($message),
            'code' => $code,
        ], $code);
    }

    protected function responseWithValidationErrors(array $errors, $message = "global.error")
    {
        $this->setLocale();
        return response()->json([
            'errors' => $errors,
            'message' => __($message),
            'code' => 422,
        ], 422);
    }

    protected function setLocale()
    {
        app()->setLocale(auth()->check() ? auth()->user()->language ?? "mm" : "mm");
    }
}
