<?php

namespace App\Providers;

use App\Services\Sms\BoomSmsService;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(SmsServiceInterface::class, function () {
            // You can use any condition here to determine which SMS gateway to use
            // For this example, we'll use Nexmo
            return new BoomSmsService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
