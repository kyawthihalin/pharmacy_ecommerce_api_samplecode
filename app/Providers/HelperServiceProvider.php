<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $dir = base_path('app/Helpers/index.php');
        require_once($dir);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
