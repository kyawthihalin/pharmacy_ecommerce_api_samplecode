<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shop_product_orders', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_id');
            $table->foreignId('shop_id');
            $table->integer('sub_total');
            $table->integer('tax');
            $table->integer('total');
            $table->timestamps();

            $table->index('invoice_id');
            $table->index('shop_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shop_product_orders');
    }
};
