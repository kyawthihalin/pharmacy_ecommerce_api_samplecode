<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shop_product_total_balances', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id');
            $table->foreignId('product_id');
            $table->foreignId('categories_id')->nullable();
            $table->integer('sale_price');
            $table->integer('low_stock_warning_quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shop_product_total_balances');
    }
};
